<?php

use WPDesk\Mutex\WordpressPostMutex;

require_once 'wpdbTrait.php';

class TestWordpressPostMutex extends WP_UnitTestCase
{

    use wpdbTrait;

    public function testFromOrder()
    {
        $order = new WC_Order();
        $order->save();

        $fromOrder = WordpressPostMutex::fromOrder($order);

        $this->assertInstanceOf(WordpressPostMutex::class, $fromOrder);
    }

    public function testAcquireLock()
    {
        $order = new WC_Order();
        $order->save();

        $fromOrder = WordpressPostMutex::fromOrder($order);

        $this->assertTrue($fromOrder->acquireLock());

        $fromOrder->releaseLock();
    }

    public function testConcurrentLock()
    {
        $order = new WC_Order();
        $order->save();

        $fromOrder = WordpressPostMutex::fromOrder($order);
        $this->assertTrue($fromOrder->acquireLock());

        global $wpdb;
        $wpdb1 = $wpdb;

        $this->newWpdb();
        $wpdb2 = $wpdb;

        $fromOrder2 = WordpressPostMutex::fromOrder($order);
        $this->assertFalse($fromOrder2->acquireLock());

        $wpdb = $wpdb1;
        $fromOrder->releaseLock();
        $wpdb->query('COMMIT');

        $wpdb = $wpdb2;
        $this->assertTrue($fromOrder2->acquireLock());
        $fromOrder2->releaseLock();

        $wpdb = $wpdb1;
    }


}
